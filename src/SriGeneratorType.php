<?php

declare(strict_types=1);


namespace MartinOlmr\SriGenerator;

/**
 * Class SriGeneratorType
 * @package MartinOlmr\SriGenerator
 */
class SriGeneratorType
{

	public const SHA_256 = 'sha256';

	public const SHA_384 = 'sha384';

	public const SHA_512 = 'sha512';
	
}