<?php

declare(strict_types=1);


namespace MartinOlmr\SriGenerator;


use Latte\CompileException;
use Latte\Compiler;
use Latte\MacroNode;
use Latte\Macros\MacroSet;
use Latte\PhpWriter;

/**
 * Class SriMacro
 * @package MartinOlmr\SriGenerator
 */
class SriMacro extends MacroSet
{

	/**
	 * @param Compiler $compiler
	 * @return SriMacro
	 */
	public static function install(Compiler $compiler)
	{
		$set = new static($compiler);

		$set->addMacro('sri', null, null, [$set, 'macroSri']);

		return $set;
	}

	/**
	 * @param string|null $url
	 * @param string $type
	 * @return string
	 * @throws SriGeneratorException
	 */
	public static function renderMacroSri(?string $url = null, string $type = SriGeneratorType::SHA_256): string
	{
		$hash = SriGenerator::generateSri($url, $type);
		return ' src="' . self::getBaseUrl() . $url . '" integrity="' . $hash . '" crossorigin="anonymous"';
	}

	/**
	 * @return string|null
	 */
	public static function getBaseUrl(): ?string
	{
		static $return;

		if ($return !== null) {
			return $return;
		}

		$currentUrl = self::getCurrentUrl();

		if ($currentUrl !== null) {
			if (preg_match('/^(https?:\/\/.+)\/www\//', $currentUrl, $localUrlParser)) {
				$return = $localUrlParser[0];
			} elseif (preg_match('/^(https?:\/\/[^\/]+)/', $currentUrl, $publicUrlParser)) {
				$return = $publicUrlParser[1];
			}
		}

		if ($return !== null) {
			$return = rtrim($return, '/');
		}

		return $return;
	}

	/**
	 * @return string|null
	 */
	public static function getCurrentUrl(): ?string
	{
		if (!isset($_SERVER['REQUEST_URI'], $_SERVER['HTTP_HOST'])) {
			return null;
		}

		return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http')
			. '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	}

	/**
	 * @param MacroNode $node
	 * @param PhpWriter $writer
	 * @return string
	 * @throws CompileException
	 */
	public function macroSri(MacroNode $node, PhpWriter $writer): string
	{
		return $writer->write(
			'echo MartinOlmr\SriGenerator\SriMacro::renderMacroSri(%node.word, %node.args)'
		);
	}
}