<?php

declare(strict_types=1);


namespace MartinOlmr\SriGenerator;

/**
 * Class SriGeneratorException
 * @package MartinOlmr\SriGenerator
 */
class SriGeneratorException extends \Exception
{

	/**
	 * @param string $fileName
	 * @param string $url
	 * @param string $path
	 * @throws SriGeneratorException
	 */
	public static function fileNotExists(string $fileName, string $url, string $path): void
	{
		throw new self(
			'File ' . $fileName . ' not found.'
			. "\n\n" . 'SRC:  ' . $url
			. "\n\n" . 'PATH: ' . $path
		);
	}

	/**
	 * @throws SriGeneratorException
	 */
	public static function externalSourceRejected(): void
	{
		throw new self('Using SriGenerator for external source is deny.');
	}

	/**
	 * @throws SriGeneratorException
	 */
	public static function missingInstance(): void
	{
		throw new self(
			'Missing instance of SriGenerator service. Have you registered it?'
			. "\n\n" . 'Install:' . "\n"
			. 'extensions:' . "\n\t" . 'sriGeneratorExtension: App\Components\Macro\SriGeneratorExtension'
		);
	}

	/**
	 * @param string $hashType
	 * @throws SriGeneratorException
	 */
	public static function unavailableHashType(string $hashType): void
	{
		throw new self(
			'SRI generator not accepted hash function ' . $hashType
			. "\n\n" . 'Available hash type are:'
			. "\n\t" . '- '
			. join("\n\t" . '- ', SriGenerator::AVAILABLE_HASH_TYPE)
		);
	}

	/**
	 * @param string $source
	 * @param string $hash
	 * @throws SriGeneratorException
	 */
	public static function canNotUseSriGeneratorForExternalSource(string $source, string $hash): void
	{
		throw new self(
			'Can not use SRI generator for external source.'
			. "\n\n" . 'Source:'
			. "\n\t" . $source
			. "\n\n" . 'File hash:'
			. "\n\t" . $hash
		);
	}

}