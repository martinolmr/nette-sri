<?php

declare(strict_types=1);


namespace MartinOlmr\SriGenerator;


use Nette\Caching\Cache;
use Nette\Caching\Storages\FileStorage;

class SriGenerator
{

	public const AVAILABLE_HASH_TYPE = ['sha256', 'sha384', 'sha512'];

	private const CACHE_KEY = 'sri-cache';

	/**
	 * @var SriGenerator
	 */
	private static $instance;

	/**
	 * @var string
	 */
	private $wwwDir;

	/**
	 * @var Cache
	 */
	private $cache;

	/**
	 * @var mixed[]
	 */
	private $data;

	/**
	 * @param string $wwwDir
	 * @param IStorage $storage
	 */
	public function __construct(string $wwwDir, IStorage $storage)
	{
		$this->wwwDir = $wwwDir;
		$this->cache = new Cache($storage, 'sri-generator');

		self::$instance = $this;
	}

	/**
	 * @param string $file
	 * @param string $type
	 * @return string
	 * @throws SriGeneratorException
	 */
	public static function generateSri(string $file, string $type = SriGeneratorType::SHA_256): string
	{
		if (self::$instance === null) {
			SriGeneratorException::missingInstance();
		}

		return self::$instance->generate($file, $type);
	}

	/**
	 * @param string $src
	 * @param string $type
	 * @return string|null
	 * @throws SriGeneratorException
	 */
	public function generate(string $src, string $type = SriGeneratorType::SHA_256): ?string
	{
		if (preg_match('/^(.*)\?.*$/', $src, $m) && isset($m[1])) {
			$src = $m[1];
		}

		$hash = $this->getHashFromCache($src, $type);
		if ($hash !== null) {
			return $hash;
		}

		$this->validateSrc($src);

		if (!(strpos($src, '/') === 0)) {
			$src .= '/' . $src;
		}

		$path = $this->wwwDir . $src;

		if (is_file($path)) {
			$hash = $this->generateHash($path, $type);
			$this->setHashToCache($src, $type, $hash);

			return $hash;
		}

		$fileName = basename($src);
		SriGeneratorException::fileNotExists($fileName, $src, $path);

		return null;
	}

	/**
	 * @param string $src
	 * @param string $type
	 * @return string|null
	 */
	private function getHashFromCache(string $src, string $type): ?string
	{
		if ($this->data === null) {
			$this->data = $this->cache->load(self::CACHE_KEY) ?? [];
		}

		return $this->data[$this->getKey($src, $type)] ?? null;
	}

	/**
	 * @param string $src
	 * @param string $type
	 * @return string
	 */
	private function getKey(string $src, string $type): string
	{
		return md5($src . $type);
	}

	/**
	 * @param string $src
	 * @throws SriGeneratorException
	 */
	private function validateSrc(string $src): void
	{
		if (preg_match('/^\w+:\/\//', $src)) {
			SriGeneratorException::canNotUseSriGeneratorForExternalSource($src, $this->generateHash($src));
		}
	}

	/**
	 * @param string $path
	 * @param string $type
	 * @return string
	 * @throws SriGeneratorException
	 */
	private function generateHash(string $path, string $type = SriGeneratorType::SHA_256): string
	{
		$this->validateHashType($type);
		$digest = base64_encode(hash_file($type, $path, true));

		return $type . '-' . $digest;
	}

	/**
	 * @param string $type
	 * @throws SriGeneratorException
	 */
	private function validateHashType(string $type): void
	{
		if (in_array($type, self::AVAILABLE_HASH_TYPE, true) === false) {
			SriGeneratorException::unavailableHashType($type);
		}
	}

	/**
	 * @param string $src
	 * @param string $type
	 * @param string $hash
	 */
	private function setHashToCache(string $src, string $type, string $hash): void
	{
		$this->data[$this->getKey($src, $type)] = $hash;

		$this->cache->save(self::CACHE_KEY, $this->data, [
			Cache::EXPIRE => '24 hours',
		]);
	}

}